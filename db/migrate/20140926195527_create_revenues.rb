class CreateRevenues < ActiveRecord::Migration
  def change
    create_table :revenues do |t|
      t.float :number
      t.integer :currency
      t.datetime :date
      t.integer :user_id
      t.integer :type
      t.integer :shop_id

      t.timestamps
    end
  end
end
