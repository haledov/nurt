class CreateMoves < ActiveRecord::Migration
  def change
    create_table :moves do |t|
      t.integer :type
      t.integer :shop_id
      t.float :total_weight
      t.float :total_cost
      t.datetime :date

      t.timestamps
    end
  end
end
