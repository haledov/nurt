class CreateRates < ActiveRecord::Migration
  def change
    create_table :rates do |t|
      t.datetime :date
      t.float :blr
      t.float :euro
      t.float :rus

      t.timestamps
    end
  end
end
