class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.integer :good_id
      t.float :price
      t.float :weight

      t.timestamps
    end
  end
end
