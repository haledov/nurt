class Revenue < ActiveRecord::Base
	belongs_to :shop
	delegate :name, to: :shop, prefix: :shop, allow_nil: false
end
