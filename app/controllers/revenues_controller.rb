class RevenuesController < ApplicationController
  before_action :set_revenue, only: [:show, :edit, :update, :destroy]

  # GET /revenues
  # GET /revenues.json
  def index
    @revenues = Revenue.all.order('date')
  end

  # GET /revenues/1
  # GET /revenues/1.json
  def show

  end

  # GET /revenues/new
  def new
    @revenue = Revenue.new
  end

  # GET /revenues/1/edit
  def edit
  end

  # POST /revenues
  # POST /revenues.json
  def create

    #@revenue = Revenue.new(revenue_params)
    puts revenue_params
    new_params = revenue_params.except(:money,:salary,:expense)

    @s = true
    REV_TYPE.each do |type|
      var = type[0]
      if revenue_params[:money][var].present?
        puts type[1].to_s+"++++"+revenue_params[:money][var]
        new_params[:retype] = type[1]
        new_params[:number] = revenue_params[:money][var]
        Rails.logger.debug "#{new_params.inspect}"
        @revenue = Revenue.new(new_params)
        @revenue.save

      end

    end
    if revenue_params[:salary].present?
        new_params[:retype] = 2
        new_params[:number] = revenue_params[:salary]
        new_params[:way] = 1
        new_params[:description] = 'salary'
        Rails.logger.debug "#{new_params.inspect}"
        @revenue = Revenue.new(new_params)
        @revenue.save
    end

    respond_to do |format|
      if @s
        format.html { redirect_to revenues_url, notice: 'Revenue was successfully created.' }
        format.json { head :no_content }
      else
        format.html { render :new }
        format.json { render json: @revenue.errors, status: :unprocessable_entity }

      end
    end
  end

  # PATCH/PUT /revenues/1
  # PATCH/PUT /revenues/1.json
  def update
    respond_to do |format|
      if @revenue.update(revenue_params)
        format.html { redirect_to @revenue, notice: 'Revenue was successfully updated.' }
        format.json { render :show, status: :ok, location: @revenue }
      else
        format.html { render :edit }
        format.json { render json: @revenue.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /revenues/1
  # DELETE /revenues/1.json
  def destroy
    @revenue.destroy
    respond_to do |format|
      format.html { redirect_to revenues_url, notice: 'Revenue was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_revenue
      @revenue = Revenue.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def revenue_params
      params[:revenue].permit(:number, :currency, :date, :retype, :shop_id, :way, :salary, :expense, money: [REV_TYPE])
    end
end
