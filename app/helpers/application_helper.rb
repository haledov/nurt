module ApplicationHelper

	def date_dmY(date)
	  if date.nil?
	    ""
	  else
	    date.strftime("%d/%m/%Y")
	  end
	end
end
